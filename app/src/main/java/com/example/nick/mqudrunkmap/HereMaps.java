package com.example.nick.mqudrunkmap;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.view.Display;

import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.common.PositioningManager.LocationMethod;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapFragment;
import com.here.android.mpa.mapping.MapState;
import com.here.android.mpa.routing.RouteManager;


import java.lang.ref.WeakReference;

public class HereMaps extends FragmentActivity {


    private Map map = null;
    private MapFragment mapFragment = null;
    private PositioningManager posManager = null;
    private boolean paused = true;
    private PositioningManager.OnPositionChangedListener positionListener;
    public GeoCoordinate currentLocation = null;

    /**
     * basic map with zoom set to MQU
     */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_here_maps);

        final MapFragment mapFragment = (MapFragment)
                getFragmentManager().findFragmentById(R.id.mapfragment);


        mapFragment.init((error) -> {
                if (error == OnEngineInitListener.Error.NONE) {
                    mapFragment.getMapGesture().addOnGestureListener(new MyOnGestureListener());

                    final Map map = mapFragment.getMap();
                    //map.setCenter(new GeoCoordinate(-33.775259, 151.112915), Map.Animation.NONE);

                    //double maxZoom = map.getMaxZoomLevel();
                    //double minZoom = map.getMinZoomLevel();

                    //map.setZoomLevel((maxZoom + minZoom) / 2);

                    //map.getPositionIndicator().setVisible(true);

                    posManager = PositioningManager.getInstance();

                    posManager.addListener(new WeakReference<>(positionListener));

                    posManager.start(PositioningManager.LocationMethod.GPS_NETWORK);
                    positionListener = new
                            PositioningManager.OnPositionChangedListener() {

                                public void onPositionUpdated(LocationMethod method,
                                                              GeoPosition position, boolean isMapMatched) {
                                    // set the center only when the app is in the foreground
                                    // to reduce CPU consumption
                                    if (!paused) {
                                        map.setCenter(position.getCoordinate(),
                                                Map.Animation.NONE);
                                        currentLocation = position.getCoordinate();
                                    }
                                }

                                public void onPositionFixChanged(LocationMethod method,
                                                                 PositioningManager.LocationStatus status) {
                                }
                            };
                    posManager.addListener(
                            new WeakReference<PositioningManager.OnPositionChangedListener>(positionListener));



                    // map fragment has been successfully initialized,
                    // now the map is ready to be used

                    map.getPositionIndicator().setVisible(true);

                    //System.out.println("FUCK " + map.getPositionIndicator());
                } else {
                    System.out.println("ERROR: Cannot initialize MapFragment");
                }

            }

        );

    }



    /**
     * My Location
     */



    // Register positioning listener

    // Resume positioning listener on wake up
    public void onResume() {
        super.onResume();
        paused = false;
        if (posManager != null) {
            posManager.start(
                    PositioningManager.LocationMethod.GPS_NETWORK);
        }
    }

    // To pause positioning listener
    public void onPause() {
        if (posManager != null) {
            posManager.stop();
        }
        super.onPause();
        paused = true;
    }

    // To remove the positioning listener
    public void onDestroy() {
        if (posManager != null) {
            // Cleanup
            posManager.removeListener(
                    positionListener);
        }
        map = null;
        super.onDestroy();
    }


}

