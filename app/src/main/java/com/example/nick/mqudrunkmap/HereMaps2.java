package com.example.nick.mqudrunkmap;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.Manifest;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.tasks.OnSuccessListener;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.common.PositioningManager.OnPositionChangedListener;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapFragment;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.routing.RouteManager;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteResult;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;


public class HereMaps2 extends AppCompatActivity
        implements
        ActivityCompat.OnRequestPermissionsResultCallback {

    private Map map;
    private MapFragment mapFragment;
    private PositioningManager posManager = null;
    private boolean paused = true;
    private OnPositionChangedListener positionListener;

    private FusedLocationProviderClient mFusedLocationClient;
    private static final int MY_PERMISSION_ACCESS_COARSE_LOCATION = 11;
    ArrayList coords = new ArrayList();
    double currentLon = 0;
    double currentLat = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_here_maps2);

        final MapFragment mapFragment = (MapFragment)
                getFragmentManager().findFragmentById(R.id.mapfragment);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions( this, new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION  },
                    MY_PERMISSION_ACCESS_COARSE_LOCATION );

            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            currentLon = location.getLatitude();
                            currentLat = location.getLongitude();

                        }

                    }

                });
        mapFragment.init(new OnEngineInitListener() {
            @Override
            public void onEngineInitializationCompleted(Error error) {
                if (error == OnEngineInitListener.Error.NONE) {
                    map = mapFragment.getMap();
                    map.setCenter(new GeoCoordinate(-33.775259, 151.112915), Map.Animation.NONE);

                    double maxZoom = map.getMaxZoomLevel();
                    double minZoom = map.getMinZoomLevel();

                    map.setZoomLevel(minZoom + 5);


                    //get current location


                                //Get destination location
                    Bundle b = getIntent().getExtras();
                    double destLon = b.getDouble("key");

                    Bundle c = getIntent().getExtras();
                    double destLat = b.getDouble("key2");
                    // Declare the rm variable (the RouteManager)
                    RouteManager rm = new RouteManager();

                    // Create the RoutePlan and add two waypoints
                    RoutePlan routePlan = new RoutePlan();
                    routePlan.addWaypoint(new GeoCoordinate(currentLon, currentLat));
                    routePlan.addWaypoint(new GeoCoordinate(destLon, destLat));

                    // Create the RouteOptions and set its transport mode & routing type
                    RouteOptions routeOptions = new RouteOptions();
                    routeOptions.setTransportMode(RouteOptions.TransportMode.PEDESTRIAN);
                    routeOptions.setRouteType(RouteOptions.Type.FASTEST);

                    routePlan.setRouteOptions(routeOptions);

                    rm.calculateRoute(routePlan, new RouteListener());
                }
            }

        });

    }

    private class RouteListener implements RouteManager.Listener {

        // Method defined in Listener
        public void onProgress(int percentage) {
            // Display a message indicating calculation progress
        }

        // Method defined in Listener
        public void onCalculateRouteFinished(RouteManager.Error error, List<RouteResult> routeResult) {
            // If the route was calculated successfully
            if (error == RouteManager.Error.NONE) {
                // Render the route on the map
                try {
                    MapRoute mapRoute = new MapRoute(routeResult.get(0).getRoute());
                    System.out.println("FUCK " + mapRoute);

                    map.addMapObject(mapRoute);

                } catch (NullPointerException e) {
                    System.out.println(e.getMessage());
                }

            } else {
                System.out.println("ERROR: Cannot initialize MapFragment");

            }
        }
    }


    /**
     * Current location
     */


}
